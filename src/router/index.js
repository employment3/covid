import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    redirect: { name: "countries" },
  },
  {
    path: '/countries',
    name: 'countries',
    component: () => import(/* webpackChunkName: "about" */ '../views/countries/index.vue')
  },
  {
    path: "/countries/:id",
    name: "countryId",
    component: () => import("../views/countries/_id.vue")
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
