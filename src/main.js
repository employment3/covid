import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import store from './store'
import router from './router'
import transport from "@/plugins/axios";
Vue.config.productionTip = false

Vue.use(transport);
Vue.prototype.$axios = transport;


new Vue({
  vuetify,
  store,
  router,
  render: h => h(App)
}).$mount('#app')
