import axios from "axios";

let config = {
  baseURL: "https://api.covid19api.com",
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  },
};

const transport = axios.create(config);
export default transport;
