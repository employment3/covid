import Vue from 'vue'
import Vuex from 'vuex'
import transport from "@/plugins/axios";
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    countriesAll: [],
    country: {
      confirmed: [],
      recovered: [],
      deaths: [],
    }
  },
  getters: {
    getAllCountries (state) {
      return state.countriesAll
    },
    getCountryGraph: (state) => (option) => {
      return state.country[option].map(a => a.Cases);
    }
  },
  mutations: {
  },
  actions: {
    async loadCountries() {
      let response = await transport.get('summary')
      this.state.countriesAll = response.data.Countries
      console.log(this.state.countriesAll)
    },
    async loadCountry({state}, payload) {
      function pad(number) {
        if (number < 10) {
          return '0' + number;
        }
        return number;
      }
      function subtractWeeks(numOfWeeks, date = new Date()) {
        date.setDate(date.getDate() - numOfWeeks * 7);

        return date;
      }

      let date = new Date()

      let fromDate = subtractWeeks(1)
      let from = `${fromDate.getFullYear()}-${pad(fromDate.getUTCMonth()+1)}-${pad(fromDate.getUTCDate())}T00:00:00Z`
      let to = `${date.getFullYear()}-${pad(date.getUTCMonth()+1)}-${pad(date.getUTCDate())}T00:00:00Z`

      let response = await transport.get(`total/country/${payload.slug}/status/${payload.case}?from=${from}&to=${to}`)
      state.country[payload.case] = response.data
    }
  },
})
